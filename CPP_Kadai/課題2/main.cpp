#include"TriangleClass.h"

int main()
{
	//TriangleClass クラスのインスタンス
	TriangleClass t;  //ここの「t」は何でもいい(int〇〇みたいなもの)

	//メンバ関数を呼び出す
	t.Input();
	t.Calc();
	t.Disp();
}