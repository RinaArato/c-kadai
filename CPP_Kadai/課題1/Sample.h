#pragma once
/*
  Sample.h
  SampleClassを宣言
*/

//ここでクラスを宣言する
class SampleClass
{
   //メンバ変数
	int a;
	int b;
	int c;
	
	//メンバ関数
	//下の「Input」「Plus」「Disp」の所は適当な文字でもOK!
public:
	void Input();
	void Plus();
	void Disp();

};