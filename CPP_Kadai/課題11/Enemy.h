#pragma once
class Enemy
{
	//メンバ変数
	int hp, atk, def;

	//メンバ関数
public:
	Enemy();
	void DispHp();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};