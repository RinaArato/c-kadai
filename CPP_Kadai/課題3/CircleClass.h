#pragma once
/*
TriangleClass.h
三角形の面積を求める TriangleClass クラスを宣言
*/

class CircleClass
{
	//メンバ変数
	float area;
	float r;

	//メンバ関数
public:
	void Input();
	void Calc();
	void Disp();
};

