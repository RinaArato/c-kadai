#include "Sub.h"

//メイン関数
int main()
{
	//Sub クラスのインスタンス（実態）を作る
	Sub a;
	//3つのメンバ関数を呼び出す
	a.Input();
	a.Minus();
	a.Disp();
}