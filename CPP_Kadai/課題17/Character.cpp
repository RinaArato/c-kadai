#include "Character.h"
#include <iostream>

//コンストラクタ（初期化）
void Character::DispHp(const char* c)
{
	std::cout << c << "HP = " << hp << "\n";
}

//攻撃
//引数：敵の防御力
//戻値：ダメージ量
int Character::Attack(int i)
{
	printf("プレイヤーの攻撃！ ");
	return atk - i / 2;
}

//ダメージを受ける（HP を減らす）
//引数：受けるダメージ量
//戻値：なし
void Character::Damage(int i, const char* c)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

//防御力を取得（アクセス関数）
//引数：なし
//戻値：防御力
int Character::GetDef()
{
	return def;
}

//戦闘不能判定
//引数：なし
//戻値：戦闘不能＝true その他＝false
bool Character::IsDead()
{
	//HP が 0 以下だったら true を返す
	if (hp <= 0)
	{
		return true;
	}
	//それ以外なら false を返す
	return false;
}