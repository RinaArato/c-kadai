#pragma once

class Character
{
protected:
	int hp, atk, def;
public:
	void DispHp(const char* c);
	int Attack(int i);
	void Damage(int i, const char* c);
	int GetDef();
	bool IsDead();
};

