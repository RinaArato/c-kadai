#include"Calclation.h"
//プロトタイプ宣言
void SetX(float a, float b);
void SetY(float a, float b);

Calclation *x,*y; //【Calclation x,y;】を【Calclation *x,*y;】にする

//メイン関数
int main()
{
	x = new Calclation; // x,y = new Calclation 2つ一緒にやってはダメ
	y = new Calclation;
	//インスタンスＸの処理
	SetX(5.0, 10.0);
	x -> Disp(); //【x.Disp();】を【x -> Disp();】にする
	delete x;    //使い終わったらdelete

	//インスタンスＹの処理
	SetY(8.0, 3.0);
	y -> Disp(); //【y.Disp();】を【y -> Disp();】にする
	delete y;    //使い終わったらdelete
}

//インスタンスＸのアクセス関数を呼ぶ
void SetX(float a, float b)
{
	x -> SetA(a); //【x.SetA(a);】を【x -> SetA(a);】にする
	x -> SetB(b); //【x.SetB(b);】を【x -> SetB(b);】にする
}

//インスタンスＹのアクセス関数を呼ぶ
void SetY(float a, float b)
{
	y -> SetA(a); //【y.SetA(a);】を【y -> SetA(a);】にする
	y -> SetB(b); //【x.SetB(b);】を【y -> SetB(b);】にする
}